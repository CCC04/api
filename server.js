//version inicial
var express = require('express'),
        app = express(),
       port = process.env.PORT || 3002;
//var fileUpload = require('express-fileupload');
//var multer     = require('multer');
//var upload   = multer({ dest: 'uploads/' });
//app.use(multer({dest:__dirname+'/home/alumno/uploads/'}).any());


var Busboy = require('busboy');
var bodyParser          = require('body-parser');
var fs                  = require('fs');
var AWS = require('aws-sdk');
var s3  = require('./s3.config.js');
var mailcomposer = require('mailcomposer');
var requestjson  = require('request-json');
var mailfrom     = 'carlosernesto.carrillo.ceron@bbva.com'
// import individual service
//var S3 = require('aws-sdk/clients/s3');
//app.use(fileUpload());
app.use(bodyParser.json({limit: '10mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))
//app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
});
var path               = require('path');
app.listen(port);
console.log('todo list RESTful API server started on: ' + port);
var urlMovimientosMlab  = 'https://api.mlab.com/api/1/databases/cccmb66208/collections/movimientos?apiKey=ZgN0848b7p6reNlW7_VWOWyDfLOuIcPP';
var urlClientesMlab     = 'https://api.mlab.com/api/1/databases/cccmb66208/collections/clientes?apiKey=ZgN0848b7p6reNlW7_VWOWyDfLOuIcPP';
var urlProductosMlab    = 'https://api.mlab.com/api/1/databases/cccmb66208/collections/productos?apiKey=ZgN0848b7p6reNlW7_VWOWyDfLOuIcPP';
var urlClientesProductosMlab  = 'https://api.mlab.com/api/1/databases/cccmb66208/collections/clientesproducto?apiKey=ZgN0848b7p6reNlW7_VWOWyDfLOuIcPP';
var urlEstadosDeCuentaTdc      = 'https://api.mlab.com/api/1/databases/cccmb66208/collections/clienteproductoestadocuenta?apiKey=ZgN0848b7p6reNlW7_VWOWyDfLOuIcPP';
var clienteMLab         = requestjson.createClient(urlMovimientosMlab);
var clienteCustomerMLab = requestjson.createClient(urlClientesMlab);
var productosMLab       = requestjson.createClient(urlProductosMlab);


// Peticiones para el API-Movimientos de mlab
app.get ("/",function(req, res) {
  res.send('GET,Cliente Recibido:' + req.params.idcliente );
  //res.sendFile(path.join(__dirname,'index.html'));
})

app.get ("/movimientos",function(req, res) {
  clienteMLab.get('',function(err,resM,body){
    if(err){console.log(body);}
    else{
      res.send(body);
    }
  });
})


app.post ("/movimientos",function(req, res) {
  clienteMLab.post('', req.body,function(err,resM,body ){
    if(err){console.log(body);}
    else{
      res.send(body);
    }
  } );
  //.res.send('POST,Cliente Recibido:' + req.body.idcliente + ' nombre'+ req.body.nombre + ' apellido'+ req.body.apellido );
})

app.get ("/movimientos/:idcliente",function(req, res) {
  var url = urlMovimientosMlab;
  if(req.params.idcliente!=undefined && req.params.idcliente!=''){
    var name = req.params.idcliente;
    name  = name.replace("idcliente", "\"idcliente\"");
    name  = name.replace("=", ":");
    url   = url + "&q={"+name+"}";

  }
  var cliprodMLab       = requestjson.createClient(url);
  cliprodMLab.get('',function(err,resM,body){
    if(err){console.log(body);}
    else{
      res.send(body);
    }
  });
})


app.get ("/clientes/:idcliente",function(req, res) {
  res.send('GET,Cliente Recibido:' + req.params.idcliente );
  //res.sendFile(path.join(__dirname,'index.html'));
})
app.post ("/",function(req, res) {
  res.send('Peticion POST, Recibida modificada en Api-CCC');
})
app.delete ("/",function(req, res) {
  res.send('Peticion DELETE, Recibida en Api-CCC');
})
app.put ("/",function(req, res) {
  res.send('Peticion PUT, Recibida en Api-CCC');
})



// SECCION PARA LOS clientes
app.get ("/clientes/:correo/:pass",function(req, res) {
  //console.log('USUARIO:' + req.params.nombre);
  //console.log('LOGIN:' + req.params.pass);
  //&q={"nombre": "Ernesto","pass":"87654321"}
  var url = urlClientesMlab;
  if(req.params.correo!=undefined && req.params.correo!=''){
    var name = req.params.correo;
    name  = name.replace("correo", "\"correo\"");
    name  = name.replace("=", ":");
    url   = url + "&q={"+name;

  }
  if(req.params.pass!=undefined && req.params.pass!=''){
    var pas = req.params.pass;
    pas  = pas.replace("pass", "\"pass\"");
    pas  = pas.replace("=", ":");
    url = url + ","+pas+"}";
  }else{
    url = url + "}";
  }
  //console.log('URL:' + url);
  var clienteLogin = requestjson.createClient(url);
  clienteLogin.get('',function(err,resM,body){
    if(err){console.log(body);}
    else{
      res.send(body);
    }
  });
})
app.post ("/clientes",function(req, res) {
  clienteCustomerMLab.post('', req.body,function(err,resM,body ){
    if(err){console.log(body);}
    else{
      res.send(body);
    }
  } );
  //.res.send('POST,Cliente Recibido:' + req.body.idcliente + ' nombre'+ req.body.nombre + ' apellido'+ req.body.apellido );
})

//SECCION DE PRODUCTOS

app.get ("/productos",function(req, res) {
  productosMLab.get('',function(err,resM,body){
    if(err){console.log(body);}
    else{
      res.send(body);
    }
  });
})
//{cliente.idcliente:"5bd8f75d22b7da1f145dd882"}
app.get ("/clientesproductos/:idcliente",function(req, res) {
  var url = urlClientesProductosMlab;


  if(req.params.idcliente!=undefined && req.params.idcliente!=''){
    var name = req.params.idcliente;
    name  = name.replace("idcliente", "\"cliente.idcliente\"");
    name  = name.replace("=", ":");
    url   = url + "&q={"+name+"}";

  }
  var cliprodMLab       = requestjson.createClient(url);
  cliprodMLab.get('',function(err,resM,body){
    if(err){console.log(body);}
    else{
      res.send(body);
    }
  });
})


// Generacion del estado de cuenta del clienteMLab
app.get ("/estadosdecuenta/:idcliente",function(req, res) {
//var estadoCuentaTdc     = requestjson.createClient(urlEstadoDeCuentaTdc);
var url = urlEstadosDeCuentaTdc;
  if(req.params.idcliente!=undefined && req.params.idcliente!=''){
    var name = req.params.idcliente;
    name  = name.replace("=", ":");
    url   = url + "&q={"+name+"}";
  }
var estadoCuentaTdc     = requestjson.createClient(url);
  estadoCuentaTdc.get('', req.body,function(err,resM,body ){
    //console.log(body);
    if(err){console.log(body);}
    else{
      res.send(body);
    }
  } );
  //.res.send('POST,Cliente Recibido:' + req.body.idcliente + ' nombre'+ req.body.nombre + ' apellido'+ req.body.apellido );
})

app.get ("/estadodecuenta/:idarchivo",function(req, res) {
//var estadoCuentaTdc     = requestjson.createClient(urlEstadoDeCuentaTdc);
var name = "";
  if(req.params.idarchivo!=undefined && req.params.idarchivo!=''){
        name = req.params.idarchivo;
        name  = name.replace(/\"+/g, '');
        name  = name.replace("=", "");
        name  = name.replace("idarchivo", "");
        name  = name + '.xml';
  }
    const s3Client = s3.s3Client;
    const params   = s3.downloadParams;
    params.Key = name;
    var response = s3Client.getObject(params)
      .createReadStream()
        .on('error', function(err){
          res.status(500).json({error:"Error -> " + err});
      }).pipe(res);


})


app.post ("/sendingestadodecuenta/:to",function(req, res) {
  var to = "";
    if(req.params.to!=undefined && req.params.to!=''){
          to = req.params.to;
          to  = to.replace(/\"+/g, '');
          to  = to.replace("=", "");
          to  = to.replace("to", "");
    }
  const ses   = s3.sESClient;
  var busboy  = new Busboy({ headers: req.headers });
  var archivo = '/home/alumno/uploads/';
  var singleFile;
  console.log(busboy);
  busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
    console.log('*************************************************************');
    console.log(file);
        singleFile = filename;
        archivo    = archivo + filename;
        file.pipe(fs.createWriteStream(archivo));
        console.log(archivo);
        const mail = mailcomposer({
            from: mailfrom,
            to: to,
            subject: 'This is a test, from the proyect of CCC',
            text: 'CORREO FROM AWS - SES To My Dear CCC',
            attachments: [
              {
                filename: singleFile,
                content: fs.createReadStream(archivo)
              },
            ],
          });
          mail.build(function(err, message){
              ses.sendRawEmail(
                {RawMessage: {Data: message}}
                , function(err, data) {
                        if(err) {res.status(500).json({error:"Error -> " + err});}
                        console.log(data);
                        /*
                      fs.unlink(archivo, (err) => {
                          if (err) {res.status(500).json({error:"Error -> " + err});}
                          console.log('successfully deleted ' + archivo);
                          console.log('Email sent:');

                        });
                        */
                     }
              );
          });
      });
      busboy.on('finish', function() {
        res.writeHead(200, { 'Connection': 'close' });
        res.end("{estatus:0}");
      });
  return req.pipe(busboy);
})


app.post ("/sendingestadodecuenta",function(req, res) {
  var to = "";
  var from = "";
  var fileBase64 = "";
  var filename = "";
    if(req.body.to!=undefined && req.body.to!=''){
          to = req.body.to;
    }
    if(req.body.from!=undefined && req.body.from!=''){
          from = req.body.from;
    }
    if(req.body.fileBase64!=undefined && req.body.fileBase64!=''){
          fileBase64 = req.body.fileBase64;
    }
    if(req.body.filename!=undefined && req.body.filename!=''){
          filename = req.body.filename;
    }
  const ses   = s3.sESClient;
  var archivo = '/home/alumno/uploads/';
  var singleFile;

  const mail = mailcomposer({
      from: from,
      to: to,
      subject: 'This is a test, from the proyect of CCC',
      text: 'CORREO FROM AWS - SES To My Dear CCC',
      attachments: [
        {
          filename: filename,
          path: fileBase64
        },
      ],
    });
    mail.build(function(err, message){
        ses.sendRawEmail(
          {RawMessage: {Data: message}}
          , function(err, data) {
                  if(err) {res.status(500).json({error:"Error -> " + err});}
                  res.writeHead(200, { 'Connection': 'close' });
                  res.end("{estatus:0}");
               }
        );
    });
})
